BindTuning SharePoint Hybrid 2019 themes include two different packages, each for installing and configuring your theme in two different CMSs: SharePoint 2019 and Office 365. 

Choose where you want to install and configure your theme first:

<a href="http://bindtuning-sharepoint-2019-themes.readthedocs.io" target="_blank">Installing and configuring the theme in SharePoint 2019</a>

<a href="http://bindtuning-office-365-themes.readthedocs.io" target="_blank">Installing and configuring the theme in Office 365</a>

